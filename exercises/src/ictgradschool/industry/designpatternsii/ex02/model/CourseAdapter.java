package ictgradschool.industry.designpatternsii.ex02.model;


import javax.swing.*;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import java.util.List;

public class CourseAdapter extends AbstractTableModel implements CourseListener  {

	private Course course;

	public CourseAdapter(Course course){
		this.course=course;
		course.addCourseListener(this);
	}

	@Override
	public void courseHasChanged(Course course) {
		fireTableDataChanged();
	}

	@Override
	public int getRowCount() {
		return course.size();
	}

	@Override
	public int getColumnCount() {
		return 7;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex){
			case 0: return course.getResultAt(rowIndex)._studentID;
			case 1: return course.getResultAt(rowIndex)._studentSurname;
			case 2: return course.getResultAt(rowIndex)._studentForename;
			case 3: return course.getResultAt(rowIndex).getAssessmentElement(StudentResult.AssessmentElement.Exam);
			case 4: return course.getResultAt(rowIndex).getAssessmentElement(StudentResult.AssessmentElement.Test);
			case 5: return course.getResultAt(rowIndex).getAssessmentElement(StudentResult.AssessmentElement.Assignment);
			case 6: return course.getResultAt(rowIndex).getAssessmentElement(StudentResult.AssessmentElement.Overall);
		}
		return null;
	}

	@Override
	public String getColumnName(int columnIndex) {
		switch (columnIndex){
			case 0: return "StudentID";
			case 1: return  "Surname";
			case 2: return "Forename";
			case 3: return "Exam";
			case 4: return "Test";
			case 5: return "Assignment";
			case 6: return "Overall";
		}
		return null;
	}

	/**********************************************************************
	 * YOUR CODE HERE
	 */
}