package ictgradschool.industry.designpatternsii.ex02.gui;

import ictgradschool.industry.designpatternsii.ex02.model.Course;
import ictgradschool.industry.designpatternsii.ex02.model.CourseListener;

//under read bar that shows the values of Average, standard deviation and Median
//An additional summary view showing key statistics: average, standard deviation and
//median scores for the course.

public class StatisticsPanelAdapter implements CourseListener {

	Course course;
	StatisticsPanel statisticsPanel;

	public StatisticsPanelAdapter(Course course, StatisticsPanel statisticsPanel) {
		this.course = course;
		this.statisticsPanel = statisticsPanel;
		course.addCourseListener(this); //add courseListener interface that we implemented
	}

	@Override
	public void courseHasChanged(Course course) {
		statisticsPanel.repaint(); //repaint on the statistics Panel every time we got changes from the course
}

	/**********************************************************************
	 * YOUR CODE HERE
	 */
	
}
