package ictgradschool.industry.designpatternsii.ex02.gui;

import ictgradschool.industry.designpatternsii.ex02.model.Course;
import ictgradschool.industry.designpatternsii.ex02.model.CourseListener;

//with red bar

public class DistributionPanelAdapter implements CourseListener {

	Course course;
	DistributionPanel distributionPanel;

	public DistributionPanelAdapter(Course course, DistributionPanel distributionPanel) {
		this.course = course;
		this.distributionPanel = distributionPanel;
		course.addCourseListener(this);//add courseListener interface that we implemented
	}

	@Override
	public void courseHasChanged(Course course) {
		distributionPanel.repaint();//repaint to the distribution Panel every time we got changes from the course
	}

	/**********************************************************************
	 * YOUR CODE HERE
	 */
}
